<?php

namespace tests;

use app\models\BitbucketRepo;
use Codeception\Test\Unit;
use Exception;

/**
 * BitbucketRepoTest contains test casess for bitbucket repo model
 * 
 * IMPORTANT NOTE:
 * All test cases down below must be implemented
 * You can add new test cases on your own
 * If they could be helpful in any form
 */
class BitbucketRepoTest extends Unit
{
    /**
     * Test case for counting repo rating
     *
     * @return void
     * @throws Exception
     */
    public function testRatingCount()
    {
        $userRepo = $this->make(BitbucketRepo::Class, ['forkCount' => 10, 'watcherCount' => 5]);
        $expectedRating = 12.5;

        $countedRating = $userRepo->getRating();

        $this->assertEquals($expectedRating, $countedRating);
    }

    /**
     * Test case for repo model data serialization
     *
     * @return void
     * @throws Exception
     */
    public function testData()
    {
        $userRepo = $this->make(BitbucketRepo::Class, ['name' => 'myRepo', 'forkCount' => 10,
                                                          'watcherCount' => 5, 'getRating' => 12.5]);
        $expectedData = [
            'name' => 'myRepo',
            'fork-count' => 10,
            'watcher-count' => 5,
            'rating' => 12.5,
        ];

        $resultData = $userRepo->getData();

        $this->assertEquals($expectedData, $resultData);
    }

    /**
     * Test case for repo model __toString verification
     *
     * @return void
     * @throws Exception
     */
    public function testStringify()
    {
        /**
         * Here assertRegExp may be used as alternative assertion.
         */
        $userRepo = $this->make(BitbucketRepo::Class, ['name' => ' my!@#$%^&*()\'\"1Repo ', 'forkCount' => 10000,
                                                                                               'watcherCount' => 500]);
        $expectedOutputString = ' my!@#$%^&*()\'\"1Repo                                                       10000 '
                                                                                                 .'⇅         500 👁️';

        $formattedString = $userRepo->__toString();

        $this->assertSame($expectedOutputString, $formattedString);
    }
}