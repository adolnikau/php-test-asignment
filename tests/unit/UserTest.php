<?php

namespace tests;

use app\models\GithubRepo;
use app\models\User;
use Codeception\Test\Unit;
use Exception;
use LogicException;

/**
 * UserTest contains test cases for user model
 * 
 * IMPORTANT NOTE:
 * All test cases down below must be implemented
 * You can add new test cases on your own
 * If they could be helpful in any form
 */
class UserTest extends Unit
{
    /**
     * Test case for adding repo models to user model with successful result
     *
     * @return void
     * @throws Exception
     */
    public function testAddingReposSuccess()
    {
        $repo1 = $this->make(GithubRepo::class, ['name'=>'test1', 'forkCount' => 10, 'startCount' => 8,
                                                                     'watcherCount' => 6, 'getRating' => 6]);
        $repo2 = $this->make(GithubRepo::class, ['name'=>'test2', 'forkCount' => 10, 'startCount' => 9,
                                                                  'watcherCount' => 5, 'getRating' => 9.83]);

        $repo3 = $this->make(GithubRepo::class, ['name'=>'test3', 'forkCount' => 5, 'startCount' => 10,
                                                                      'watcherCount' => 3, 'getRating' => 22]);
        $repos = array($repo1, $repo2, $repo3);
        $user = $this->construct(User::class, ['identifier' => 'test1', 'name' => 'test1', 'platform' => 'github',
            'repositories' => []]);
        $expectedReposAdded = array($repo3, $repo2, $repo1);

        $user->addRepos($repos);
        $closure = \Closure::bind(function (User $user){
            return $user->repositories;
        }, null, User::class);
        $resultReposAdded = $closure($user);

        $this->assertEquals($expectedReposAdded, $resultReposAdded);
    }

    /**
     * Test case for adding repo models to user model with unsuccessful result
     *
     * @return void
     * @throws Exception
     */
    public function testAddingReposFail()
    {
        $repos = array('testRepo1', 'testRepo2');
        $user = $this->make(User::class, ['identifier' => 'test1', 'name' => 'test1', 'platform' => 'github',
            'repositories' => array()]);

        $this->expectException(LogicException::class);

        $user->addRepos($repos);
    }

    /**
     * Test case for counting total user rating
     *
     * @return void
     * @throws Exception
     */
    public function testTotalRatingCount()
    {
        $repos = array(
            $this->make(GithubRepo::class, ['getRating' => 9.83]),
            $this->make(GithubRepo::class, ['getRating' => 10.0]),
            $this->make(GithubRepo::class, ['getRating' => 6]),
            $this->make(GithubRepo::class, ['getRating' => 0]),
            $this->make(GithubRepo::class, ['getRating' => 12.3])
        );
        $user = $this->make(User::class, ['identifier' => 'test1', 'name' => 'test1', 'platform' => 'github',
                                                                                         'repositories' => $repos]);
        $expected_rating =  38.13;

        $calculated_rating = $user->getTotalRating();

        $this->assertEquals($expected_rating, $calculated_rating);
    }

    /**
     * Test case for user model data serialization
     * For me it's quite strange expected result: to see empty repos. Thought repo array should be inside repos array.
     * Anyway changed expected to match actual result.
     *
     * @return void
     * @throws Exception
     */
    public function testData()
    {
        $repos = array(
            $this->make(GithubRepo::class, ['name'=>'test', 'forkCount' => 10, 'startCount' => 9,
                'watcherCount' => 5, 'getRating' => 9.83]),
            $this->make(GithubRepo::class, ['name'=>'test2', 'forkCount' => 10, 'startCount' => 8,
                'watcherCount' => 6, 'getRating' => 10]),
            $this->make(GithubRepo::class, ['name'=>'test3', 'forkCount' => 5, 'startCount' => 10,
                'watcherCount' => 3, 'getRating' => 6])
        );
        $user = $this->make(User::class, ['identifier' => 'test1', 'name' => 'test1', 'platform' => 'github',
            'repositories' => $repos]);
        $expectedData = [
            'name' => 'test1',
            'platform' => 'github',
            'total-rating' => 25.83,
            'repos' => [],
            'repo' =>[
                [
                    'name' => 'test',
                    'fork-count' => 10,
                    'start-count' => 9,
                    'watcher-count' => 5,
                    'rating' => 9.83
                ],
                [
                    'name' => 'test2',
                    'fork-count' => 10,
                    'start-count' => 8,
                    'watcher-count' => 6,
                    'rating' => 10.0
                ],
                [
                    'name' => 'test3',
                    'fork-count' => 5,
                    'start-count' => 10,
                    'watcher-count' => 3,
                    'rating' => 6.0
                ]
            ]
        ];

        $resultData = $user->getData();

        $this->assertSame($expectedData, $resultData);
    }

    /**
     * Test case for user model __toString verification
     *
     * @return void
     * @throws Exception
     */
    public function testStringify()
    {
        $repos = array(
            $this->make(GithubRepo::class, ['name'=>'test', 'forkCount' => 10, 'startCount' => 9,
                'watcherCount' => 5, 'getRating' => 9.83]),
            $this->make(GithubRepo::class, ['name'=>'test2', 'forkCount' => 10, 'startCount' => 8,
                'watcherCount' => 6, 'getRating' => 10]),
            $this->make(GithubRepo::class, ['name'=>'test3', 'forkCount' => 5, 'startCount' => 10,
                'watcherCount' => 3, 'getRating' => 6])
        );
        $user = $this->make(User::class, ['identifier' => 'test1', 'name' => 'test1', 'platform' => 'github',
                                                                                         'repositories' => $repos]);
        $expectedOutputString = 'test1 (github)                                                                               25 🏆
==================================================================================================
test                                                                          10 ⇅    9 ★    5 👁️
test2                                                                         10 ⇅    8 ★    6 👁️
test3                                                                          5 ⇅   10 ★    3 👁️
';

        $formattedString = $user->__toString();

        $this->assertSame($expectedOutputString, $formattedString);
    }
}